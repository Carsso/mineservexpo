server 'nina.corleone.fr', :app, :web, :primary => true
set :branch, "master"
set :deploy_to, "/var/www/mineserv-exposition.fr/web"
set :user, "carssomse"

set :composer_options, "--no-interaction --verbose --prefer-dist --optimize-autoloader --no-progress"
logger.level = Logger::MAX_LEVEL

before "symfony:cache:warmup", "symfony:doctrine:schema:update"