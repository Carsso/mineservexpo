set :stages, ["production"]
set :default_stage, "production"
set :stage_dir, "app/config"
require 'capistrano/ext/multistage'

set :application, "MineServ Exposition"
set :app_path, "app"

set :repository,  "git@bitbucket.org:Carsso/mineservexpo.git"
set :scm,         :git

set :use_sudo, false
set :group_writable, true
set :model_manager, "doctrine"
set :dump_assetic_assets, true
set :assets_symlinks, true

set :shared_files,      ["app/config/parameters.yml"]
set :shared_children,     [app_path + "/logs", web_path + "/uploads", "vendor", app_path + "/var/sessions"]
set :use_composer, true
set :deploy_via, :copy
set :copy_exclude, [".git/*", "scripts/*"]
set :keep_releases,  3