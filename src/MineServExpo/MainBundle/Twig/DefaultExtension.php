<?php

namespace MineServExpo\MainBundle\Twig;

class DefaultExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'gravatar_hash' => new \Twig_Filter_Method($this, 'gravatarHash'),
        );
    }

    public function gravatarHash($email_address)
    {
        return md5(strtolower(trim($email_address)));
    }

    public function getName()
    {
        return 'default_extension';
    }
}