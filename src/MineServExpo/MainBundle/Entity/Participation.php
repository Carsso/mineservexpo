<?php

namespace MineServExpo\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Participation
 *
 * @ORM\Table(name="participation")
 * @ORM\Entity(repositoryClass="MineServExpo\MainBundle\Entity\ParticipationRepository")
 */
class Participation
{

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MineServExpo\UserBundle\Entity\User", inversedBy="participations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var $slug
     * @Gedmo\Slug(fields={"name"}, updatable=true, unique=true)
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="banner", type="string", length=255)
     */
    private $banner;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status = "0";

    /**
     * @var string
     *
     * @ORM\Column(name="status_detail", type="string", length=255, nullable=true)
     */
    private $statusDetail;

    /**
     * @var integer
     *
     * @ORM\Column(name="server_type", type="integer", nullable=true)
     */
    private $serverType;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube", type="string", length=255, nullable=true)
     */
    private $youtube;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var integer
     *
     * @ORM\Column(name="audio_type", type="integer")
     */
    private $audioType;

    /**
     * @var string
     *
     * @ORM\Column(name="audio", type="string", length=255, nullable=true)
     */
    private $audio;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $schematicName;

    /**
     * @Assert\File(maxSize="250k")
     */
    public $file;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Participation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Participation
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Participation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set banner
     *
     * @param string $banner
     * @return Participation
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return string
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Participation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    public function getTypeText()
    {
        switch ($this->type) {
            case 1:
                $result = 'Youtubeur';
                break;
            case 2:
                $result = 'Site Web';
                break;
            case 0:
            default:
                $result = 'Serveur';
                break;
        }
        return $result;
    }

    /**
     * Set serverType
     *
     * @param integer $serverType
     * @return Participation
     */
    public function setServerType($serverType)
    {
        $this->serverType = $serverType;

        return $this;
    }

    /**
     * Get serverType
     *
     * @return integer
     */
    public function getServerType()
    {
        return $this->serverType;
    }

    public function getServerTypeText()
    {
        switch ($this->serverType) {
            case 0:
                $result = 'Industriel';
                break;
            case 1:
                $result = 'P.V.P.';
                break;
            case 2:
                $result = 'R.P.';
                break;
            case 3:
                $result = 'Semi-R.P.';
                break;
            case 4:
                $result = 'Free Build/Créatif';
                break;
            case 5:
                $result = 'Survie';
                break;
            case 6:
                $result = 'Autre';
                break;
            default:
                $result = '';
                break;
        }
        return $result;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Participation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusText()
    {
        switch ($this->status) {
            case 1:
                $result = 'Non valide';
                break;
            case 2:
                $result = 'Accepté';
                break;
            case 3:
                $result = 'Refusé';
                break;
            case 0:
            default:
                $result = 'En attente';
                break;
        }
        return $result;
    }

    public function isEditable($user)
    {
        if ($this->isOwner($user) && $this->status <= 1) {
            return true;
        }
        return false;
    }

    public function isOwner($user)
    {
        if ($user && is_object($user) && $this->user->getId() == $user->getId()) {
            return true;
        }
        return false;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Participation
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set youtube
     *
     * @param string $youtube
     * @return Participation
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Get youtube
     *
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Participation
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Participation
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set audioType
     *
     * @param integer $audioType
     * @return Participation
     */
    public function setAudioType($audioType)
    {
        $this->audioType = $audioType;

        return $this;
    }

    /**
     * Get audioType
     *
     * @return integer
     */
    public function getAudioType()
    {
        return $this->audioType;
    }

    /**
     * Set audio
     *
     * @param string $audio
     * @return Participation
     */
    public function setAudio($audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return string
     */
    public function getAudio()
    {
        return $this->audio;
    }


    /**
     * Set user
     *
     * @param \MineServExpo\UserBundle\Entity\User $user
     * @return Participation
     */
    public function setUser(\MineServExpo\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MineServExpo\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set statusDetail
     *
     * @param string $statusDetail
     * @return Participation
     */
    public function setStatusDetail($statusDetail)
    {
        $this->statusDetail = $statusDetail;

        return $this;
    }

    /**
     * Get statusDetail
     *
     * @return string
     */
    public function getStatusDetail()
    {
        return $this->statusDetail;
    }

    /**
     * Set schematicName
     *
     * @param string $schematicName
     * @return Participation
     */
    public function setSchematicName($schematicName)
    {
        $this->schematicName = $schematicName;

        return $this;
    }

    /**
     * Get schematicName
     *
     * @return string
     */
    public function getSchematicName()
    {
        return $this->schematicName;
    }


    public function getWebPath()
    {
        return null === $this->schematicName ? null : $this->getUploadDir() . '/' . $this->schematicName;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/schematics';
    }

    public function uploadSchematic()
    {
        if($this->schematicName){
            $old_file = $this->getUploadRootDir().'/'.$this->schematicName;
            if(is_file($old_file)){
                unlink($old_file);
            }
        }
        $new_file_name = $this->slug . '-'.date('d-m-Y-H-i-s').'.schematic';
        $file_name = $this->file->getClientOriginalName();
        $pathinfo = pathinfo($file_name);
        if (strtolower($pathinfo['extension']) == 'schematic') {
            $this->file->move($this->getUploadRootDir(), $new_file_name);
            $this->schematicName = $new_file_name;
            $this->file = null;
            return $this->schematicName;
        } else {
            return false;
        }
    }
}
