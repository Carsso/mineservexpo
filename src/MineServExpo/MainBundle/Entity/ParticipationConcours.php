<?php

namespace MineServExpo\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ParticipationConcours
 *
 * @ORM\Table(name="participation_concours")
 * @ORM\Entity(repositoryClass="MineServExpo\MainBundle\Entity\ParticipationConcoursRepository")
 */
class ParticipationConcours
{

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="MineServExpo\UserBundle\Entity\User", inversedBy="participation_concours")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var $slug
     * @Gedmo\Slug(fields={"name"}, updatable=true, unique=true)
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="username2", type="string", length=255, nullable=true)
     */
    private $username2;

    /**
     * @var string
     *
     * @ORM\Column(name="username3", type="string", length=255, nullable=true)
     */
    private $username3;

    /**
     * @var string
     *
     * @ORM\Column(name="username4", type="string", length=255, nullable=true)
     */
    private $username4;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \MineServExpo\UserBundle\Entity\User $user
     * @return ParticipationConcours
     */
    public function setUser(\MineServExpo\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MineServExpo\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ParticipationConcours
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ParticipationConcours
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set username2
     *
     * @param string $username2
     * @return ParticipationConcours
     */
    public function setUsername2($username2)
    {
        $this->username2 = $username2;

        return $this;
    }

    /**
     * Get username2
     *
     * @return string 
     */
    public function getUsername2()
    {
        return $this->username2;
    }

    /**
     * Set username3
     *
     * @param string $username3
     * @return ParticipationConcours
     */
    public function setUsername3($username3)
    {
        $this->username3 = $username3;

        return $this;
    }

    /**
     * Get username3
     *
     * @return string 
     */
    public function getUsername3()
    {
        return $this->username3;
    }

    /**
     * Set username4
     *
     * @param string $username4
     * @return ParticipationConcours
     */
    public function setUsername4($username4)
    {
        $this->username4 = $username4;

        return $this;
    }

    /**
     * Get username4
     *
     * @return string 
     */
    public function getUsername4()
    {
        return $this->username4;
    }
}
