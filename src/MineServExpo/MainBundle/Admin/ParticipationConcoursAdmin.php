<?php

namespace MineServExpo\MainBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;

class ParticipationConcoursAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array(
                'required' => true,
                'label' => 'Nom de l\'équipe',
            ))
            ->add('user', 'entity', array(
                'class' => 'MineServExpoUserBundle:User',
                'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.username', 'ASC');
                    },
                'label' => 'Joueur 1',
            ))
            ->add('username2', 'textarea', array(
                'required' => true,
                'label' => 'Joueur 2',
            ))
            ->add('username3', 'textarea', array(
                'required' => true,
                'label' => 'Joueur 3',
            ))
            ->add('username4', 'textarea', array(
                'required' => true,
                'label' => 'Joueur 4',
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array(
                'label' => 'Nom de l\'équipe',
            ))
            ->add('user',
                null,
                array(
                    'label' => 'Joueur 1',
                ),
                null,
                array(
                    'multiple' => true,
                )
            );
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', 'text', array(
                'label' => 'Nom de l\'équipe',
            ))
            ->add('user', 'entity', array(
                'label' => 'Joueur 1',
            ))
            ->add('username2', 'textarea', array(
                'required' => true,
                'label' => 'Joueur 2',
            ))
            ->add('username3', 'textarea', array(
                'required' => true,
                'label' => 'Joueur 3',
            ))
            ->add('username4', 'textarea', array(
                'required' => true,
                'label' => 'Joueur 3',
            ));
    }
}