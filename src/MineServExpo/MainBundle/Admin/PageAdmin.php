<?php

namespace MineServExpo\MainBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;


class PageAdmin extends Admin
{
    protected $translationDomain = 'SonataAdminBundle';

    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Général')
            ->add('name')
            ->add('is_active')
            ->add('Contenu', NULL, array('template' => 'MineServExpoMainBundle:PageAdmin:content.html.twig'))
            ->end()
            ->with('Métas')
            ->add('meta_title')
            ->add('meta_description')
            ->add('meta_keyword')
            ->end()
        ;
    }



    public function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Général')
            ->add('name')
            ->add('slug', null, array('required' => false))
            ->add('content', 'textarea', array('required' => false, 'attr'=>array('class'=>'tinymce','data-theme' => 'advanced')))
            ->add('is_active')
            ->end()
            ->with('Métas')
            ->add('meta_title')
            ->add('meta_description')
            ->add('meta_keyword')
            ->end()
            ->setHelps(array(
                'meta_keyword' => 'séparer les différents mots clés par une virgule',
            ))
        ;
    }

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('slug', null, array('label'=>'Url'))
            ->add('is_active', 'boolean')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array('template' => 'MineServExpoMainBundle:PageAdmin:view-link.html.twig'),
                )
            ))
        ;
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('is_active')
        ;
    }

    /**
     * @param string $name
     *
     * @return null|string
     */
    public function getTemplate($name)
    {
        if($name=="edit"){
            return "MineServExpoMainBundle:PageAdmin:edit.html.twig";
        }
        else if (isset($this->templates[$name])) {
            return $this->templates[$name];
        }
        return null;
    }
}