<?php

namespace MineServExpo\MainBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;

class ParticipationAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('type', 'choice', array(
                'choices' => array(
                    '0' => 'Serveur',
                    '1' => 'Youtubeur',
                    '2' => 'Site Web',
                ),
                'empty_value' => false,
                'required' => true,
                'label' => 'Type d\'inscription',
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    '0' => 'En attente',
                    '1' => 'Non valide',
                    '2' => 'Accepté',
                    '3' => 'Refusé',
                ),
                'empty_value' => false,
                'required' => true,
                'label' => 'Statut',
            ))
            ->add('statusDetail', 'text', array(
                'required' => false,
                'label' => 'Commentaires du statut (sera affiché à l\'utilisateur)',
            ))
            ->add('user', 'entity', array(
                'class' => 'MineServExpoUserBundle:User',
                'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.username', 'ASC');
                    },
                'label' => 'Propriétaire',
            ))
            ->add('name', 'text', array(
                'required' => true,
                'label' => 'Nom',
            ))
            ->add('description', 'textarea', array(
                'required' => true,
                'label' => 'Description',
            ))
            ->add('banner', 'text', array(
                'required' => true,
                'label' => 'URL de votre bannière en 530px*150px (elle sera utilisée sur le site)',
            ))
            ->add('serverType', 'choice', array(
                'choices' => array(
                    '0' => 'Industriel',
                    '1' => 'P.V.P.',
                    '2' => 'R.P.',
                    '3' => 'Semi-R.P.',
                    '4' => 'Free Build/Créatif',
                    '5' => 'Survie',
                    '6' => 'Autre',
                ),
                'empty_value' => false,
                'required' => false,
                'label' => 'Type de serveur',
            ))
            ->add('website', 'text', array(
                'required' => false,
                'label' => 'Site internet (sans http://)',
                'attr' => array(
                    'placeholder' => 'ex: www.monsite.com',
                ),
            ))
            ->add('youtube', 'text', array(
                'required' => false,
                'label' => 'URL de la chaine Youtube (Pseudo seulement)',
                'attr' => array(
                    'placeholder' => 'ex: machaine (pour http://www.youtube.com/machaine)',
                ),
            ))
            ->add('facebook', 'text', array(
                'required' => false,
                'label' => 'URL de la page Facebook (Alias seulement)',
                'attr' => array(
                    'placeholder' => 'ex: mapage (pour https://www.facebook.com/mapage)',
                ),
            ))
            ->add('twitter', 'text', array(
                'required' => false,
                'label' => 'URL du profil Twitter (Pseudo seulement)',
                'attr' => array(
                    'placeholder' => 'ex: monprofil (pour http://twitter.com/monprofil)',
                ),
            ))
            ->add('audioType', 'choice', array(
                'choices' => array(
                    '0' => 'Teamspeak',
                    '1' => 'Mumble',
                ),
                'empty_value' => false,
                'required' => false,
                'label' => 'Type de serveur audio',
            ))
            ->add('audio', 'text', array(
                'required' => false,
                'label' => 'IP du serveur audio',
                'attr' => array(
                    'placeholder' => 'ex: ts.monsite.com:12345',
                ),
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array(
                'label' => 'Nom',
            ))
            ->add('type',
                'doctrine_orm_choice',
                array(
                    'label' => 'Type d\'inscription',
                ),
                'choice',
                array(
                    'choices' => array(
                        '0' => 'Serveur',
                        '1' => 'Youtubeur',
                        '2' => 'Site Web',
                    ),
                    'multiple' => true,
                ))
            ->add('serverType',
                'doctrine_orm_choice',
                array(
                    'label' => 'Type de serveur',
                ),
                'choice',
                array(
                    'choices' => array(
                        '0' => 'Industriel',
                        '1' => 'P.V.P.',
                        '2' => 'R.P.',
                        '3' => 'Semi-R.P.',
                        '4' => 'Free Build/Créatif',
                        '5' => 'Survie',
                        '6' => 'Autre',
                    ),
                    'multiple' => true,
                ))
            ->add('status',
                'doctrine_orm_choice',
                array(
                    'label' => 'Statut',
                ),
                'choice',
                array(
                    'choices' => array(
                        '0' => 'En attente',
                        '1' => 'Non valide',
                        '2' => 'Accepté',
                        '3' => 'Refusé',
                    ),
                    'multiple' => true,
                ))
            ->add('user',
                null,
                array(
                    'label' => 'Propriétaire',
                ),
                null,
                array(
                    'multiple' => true,
                )
            );
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', 'text', array(
                'label' => 'Nom',
            ))
            ->add('type', 'choice', array(
                'choices' => array(
                    '0' => 'Serveur',
                    '1' => 'Youtubeur',
                    '2' => 'Site Web',
                ),
                'label' => 'Type d\'inscription',
            ))
            ->add('serverType', 'choice', array(
                'choices' => array(
                    '0' => 'Industriel',
                    '1' => 'P.V.P.',
                    '2' => 'R.P.',
                    '3' => 'Semi-R.P.',
                    '4' => 'Free Build/Créatif',
                    '5' => 'Survie',
                    '6' => 'Autre',
                ),
                'label' => 'Type de serveur',
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    '0' => 'En attente',
                    '1' => 'Non valide',
                    '2' => 'Accepté',
                    '3' => 'Refusé',
                ),
                'label' => 'Statut',
            ))
            ->add('user', 'entity', array(
                'label' => 'Propriétaire',
            ))
            ->add('schematicName', 'text', array(
                'label' => 'Schematic',
            ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array('template' => 'MineServExpoMainBundle:ParticipationAdmin:view-link.html.twig'),
                )
            ))
        ;
    }
}