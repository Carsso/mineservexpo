<?php

namespace MineServExpo\MainBundle\Controller;

use MineServExpo\MainBundle\Entity\Participation;
use MineServExpo\MainBundle\Entity\ParticipationConcours;
use MineServExpo\MainBundle\Form\ParticipationType;
use MineServExpo\MainBundle\Form\ParticipationConcoursType;
use MineServExpo\MainBundle\Form\SchematicType;
use MineServExpo\MainBundle\Form\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction()
    {
        $partners = $this->getPartners();
        $partners1 = array();
        $partners2 = array();

        $tmp_partner = false;
        foreach ($partners as $partner_key => $partner_value) {
            if ($tmp_partner) {
                $tmp_partner = false;
                $partners1[$partner_key] = $partner_value;
            } else {
                $tmp_partner = true;
                $partners2[$partner_key] = $partner_value;
            }
        }

        $page = $this->getDoctrine()
            ->getManager()
            ->getRepository('MineServExpoMainBundle:Page')
            ->findOneBy(array('slug' => 'homepage'));

        if (!$page) {
            $content = '';
        } else {
            $seoPage = $this->container->get('sonata.seo.page');
            if ($page->getMetaTitle()) {
                $title = $page->getMetaTitle();
            } else {
                $title = 'MineServ Exposition - ' . $page->getName();
            }
            if ($page->getMetaDescription()) {
                $description = $page->getMetaDescription();
            } else {
                $description = html_entity_decode(preg_replace("/\s\s+/", "", strip_tags($page->getContent())));
            }
            $seoPage->setTitle($title)
                ->addMeta('name', 'description', $description)
                ->addMeta('property', 'og:title', $title)
                ->addMeta('property', 'og:description', $description);
            $content = $page->getContent();
        }
        return array('content' => $content, 'partners1' => $partners1, 'partners2' => $partners2);
    }

    /**
     * @Route("/exposants", name="exposants")
     * @Route("/exposants/{t}/{q}", name="exposants_search", options={"expose"=true})
     * @Template()
     */
    public function exposantsAction(Request $request, $t = 'all', $q = '')
    {
        switch ($t) {
            case 'serveurs':
                $type = 0;
                break;
            case 'youtubeurs':
                $type = 1;
                break;
            case 'sites-web':
                $type = 2;
                break;
            default:
                $type = null;
                break;
        }

        $repository = $this->getDoctrine()
            ->getRepository('MineServExpoMainBundle:Participation');

        $queryBuilder = $repository->createQueryBuilder('p');
        $queryBuilder->where('p.status = 2');
        if (!empty($q)) {
            $queryBuilder->andWhere('p.name LIKE :query')->setParameter('query', '%' . $q . '%');
        }
        if ($type !== null) {
            $queryBuilder->andWhere('p.type = :type')->setParameter('type', (int)$type);
        }
        $queryBuilder->orderBy('p.name');

        $query = $queryBuilder->getQuery();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate($query, $this->get('request')->query->get('page', 1), 10);

        return array('pagination' => $pagination, 'q' => $q, 't' => $t);
    }

    /**
     * @Route("/participer", name="participer")
     * @Template()
     */
    public function participerAction()
    {
        $participations = array();
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $participations = $this->getUser()->getParticipations();
        }
        return array('participations' => $participations);
    }

    /**
     * @Route("/participations", name="participations")
     * @Template()
     */
    public function participationsAction()
    {
        $participations = array();
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $participations = $this->getUser()->getParticipations();
        }
        return array('participations' => $participations);
    }

    /**
     * @Route("/participation/{slug}", name="participation")
     * @Template()
     */
    public function participationAction($slug)
    {
        $participation = $this->getDoctrine()
            ->getRepository('MineServExpoMainBundle:Participation')
            ->findOneBy(array('slug' => $slug));
        return array('participation' => $participation);
    }

    /**
     * @Route("/participer/serveur", name="participer_serveur")
     * @Template("MineServExpoMainBundle:Default:participerForm.html.twig")
     */
    public function participerServeurAction(Request $request)
    {
        return $this->participerForm($request, 0);
    }

    /**
     * @Route("/participer/youtubeur", name="participer_youtubeur")
     * @Template("MineServExpoMainBundle:Default:participerForm.html.twig")
     */
    public function participerYoutubeAction(Request $request)
    {
        return $this->participerForm($request, 1);
    }

    /**
     * @Route("/participer/site-web", name="participer_site_web")
     * @Template("MineServExpoMainBundle:Default:participerForm.html.twig")
     */
    public function participerSiteWebAction(Request $request)
    {
        return $this->participerForm($request, 2);
    }

    /**
     * @Route("/participer/edit/{id}", name="participer_edit")
     * @Template("MineServExpoMainBundle:Default:participerForm.html.twig")
     */
    public function participerEditAction(Request $request, $id)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $participation = $this->getDoctrine()
                ->getRepository('MineServExpoMainBundle:Participation')
                ->find($id);
            if ($participation && $participation->getUser() && $participation->getUser()->getId() == $this->getUser()->getId()) {
                return $this->participerForm($request, 2, $participation);
            } else {
                throw new AccessDeniedException();
            }
        } else {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }

    /**
     * @Route("/participer/schematic/{id}", name="participer_schematic")
     * @Template()
     */
    public function participerSchematicAction(Request $request, $id)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $participation = $this->getDoctrine()
                ->getRepository('MineServExpoMainBundle:Participation')
                ->find($id);
            if ($participation && $participation->getUser() && $participation->getUser()->getId() == $this->getUser()->getId()) {
                $form = $this->createForm(new SchematicType(), $participation);
                if ($request->isMethod('post')) {
                    $form->handleRequest($request);
                    if ($form->isValid()) {
                        $em = $this->getDoctrine()->getManager();

                        if ($participation->uploadSchematic()) {
                            $em->persist($participation);
                            $em->flush();
                        }
                    }
                }
                return array('form' => $form->createView(), 'participation' => $participation);
            } else {
                throw new AccessDeniedException();
            }
        } else {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }

    public function participerForm(Request $request, $type, $participation = null)
    {
        if (is_null($participation)) {
            $participation = new Participation();
            if (!$request->isMethod('post')) {
                $participation->setType($type);
            }
        }
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $form = $this->createForm(new ParticipationType(), $participation);

            if ($request->isMethod('post')) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if (!$participation->getUser()) {
                        $participation->setUser($this->getUser());
                    }
                    $participation->setStatus(0);
                    $participation->setStatusDetail(null);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($participation);
                    $em->flush();

                    return $this->redirect($this->generateUrl('participations'));
                }
            }
            return array('form' => $form->createView());
        } else {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }

    /**
     * @Route("/concours", name="concours")
     * @Template("MineServExpoMainBundle:Default:concours.html.twig")
     */
    public function participerConcoursAction(Request $request)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $participation = $this->getUser()->getParticipationConcours();
            if ($participation) {
                return $this->redirect($this->generateUrl('concours_edit', array('id' => $participation->getId())));
            } else {
                return $this->participerConcoursForm($request);
            }
        } else {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }

    /**
     * @Route("/concours/edit/{id}", name="concours_edit")
     * @Template("MineServExpoMainBundle:Default:concours.html.twig")
     */
    public function participerConcoursEditAction(Request $request, $id)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $participation = $this->getDoctrine()
                ->getRepository('MineServExpoMainBundle:ParticipationConcours')
                ->find($id);
            if ($participation && $participation->getUser() && $participation->getUser()->getId() == $this->getUser()->getId()) {
                return $this->participerConcoursForm($request, $participation);
            } else {
                throw new AccessDeniedException();
            }
        } else {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }

    public function participerConcoursForm(Request $request, $participation = null)
    {
        if (is_null($participation)) {
            $participation = new ParticipationConcours();
        }
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $form = $this->createForm(new ParticipationConcoursType(), $participation);

            if ($request->isMethod('post')) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if (!$participation->getUser()) {
                        $participation->setUser($this->getUser());
                    }
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($participation);
                    $em->flush();

                    return $this->redirect($this->generateUrl('concours_edit', array('id' => $participation->getId())));
                }
            }

            $page = $this->getDoctrine()
                ->getManager()
                ->getRepository('MineServExpoMainBundle:Page')
                ->findOneBy(array('slug' => 'reglement-concours'));

            if (!$page) {
                $content = '';
                $title = '';
            } else {
                $content = $page->getContent();
                $title = $page->getName();
            }
            return array('form' => $form->createView(), 'participation' => $participation, 'title' => $title, 'content' => $content);
        } else {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }

    /**
     * @Route("/page/{slug}", name="page")
     * @Template()
     */
    public function pageAction($slug)
    {
        $page = $this->getDoctrine()
            ->getManager()
            ->getRepository('MineServExpoMainBundle:Page')
            ->findOneBy(array('slug' => $slug));

        if (!$page) {
            throw $this->createNotFoundException();
        } else {
            $seoPage = $this->container->get('sonata.seo.page');
            if ($page->getMetaTitle()) {
                $title = $page->getMetaTitle();
            } else {
                $title = 'MineServ Exposition - ' . $page->getName();
            }
            if ($page->getMetaDescription()) {
                $description = $page->getMetaDescription();
            } else {
                $description = html_entity_decode(preg_replace("/\s\s+/", "", strip_tags($page->getContent())));
            }
            $seoPage->setTitle($title)
                ->addMeta('name', 'description', $description)
                ->addMeta('property', 'og:title', $title)
                ->addMeta('property', 'og:description', $description);
            return array('title' => $page->getName(), 'content' => $page->getContent());
        }
    }


    /**
     * @Route("/update-all-slugs", name="update-all-slugs")
     * @Template()
     */
    public function updateAllSlugsAction()
    {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $em = $this->get('doctrine.orm.entity_manager');
            $participations = $this->getDoctrine()
                ->getRepository('MineServExpoMainBundle:Participation')
                ->findAll();
            foreach ($participations as $participation) {
                $participation->setSlug(null);
                $em->persist($participation);
            }
            $em->flush();
            return new JsonResponse('DONE !');
        } else {
            throw new AccessDeniedException();
        }
    }


    public function shuffle_assoc(&$array)
    {
        $keys = array_keys($array);
        shuffle($keys);
        foreach ($keys as $key) {
            $new[$key] = $array[$key];
        }
        $array = $new;
        return true;
    }

    public function getPartners()
    {
        $partners = array(
            'dedizones' => 'https://client.dedizones.com/link.php?id=40',
            'holycube' => 'https://twitter.com/HolyCube',
            'languagecraft' => 'http://languagecraft.tv',
            'ironcraft' => 'http://www.ironcraft.fr',
            'creative_community' => 'http://www.creativecommunity.fr',
            'new_heaven' => 'http://teamnewheaven.fr',
        );

        $this->shuffle_assoc($partners);
        return $partners;
    }


    /**
     * @Route("/redirect/{url}", name="redirect", options={"expose"=true})
     * @Template()
     */
    public function redirectAction(Request $request, $url)
    {
        $partners = $this->getPartners();
        return array('url' => $url, 'url_decoded' => urldecode($url), 'partners' => $partners);
    }


    public function headerAction()
    {
        return $this->container->get('templating')->renderResponse('MineServExpoMainBundle:Default:header.html.twig', array());
    }
}
