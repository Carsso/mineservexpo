<?php

namespace MineServExpo\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class MinecraftController extends Controller
{
    /**
     * @Route("/minecraft-premium", name="minecraft_premium", options={"expose"=true})
     * @Route("/minecraft-premium/{user}", name="minecraft_premium_user", options={"expose"=true})
     * @Template()
     */
    public function indexAction($user = 'char')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://minecraft.net/haspaid.jsp?user=' . $user);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $output = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($status != '200') {
            $return = -1;
        } else {
            switch ($output) {
                case 'true':
                    $return = 1;
                    break;
                case 'false':
                    $return = 0;
                    break;
                default:
                    $return = -1;
                    break;
            }
        }
        return new JsonResponse(array('pseudo' => $user, 'result' => $return));
    }
}
