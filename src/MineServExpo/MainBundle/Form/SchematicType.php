<?php

namespace MineServExpo\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SchematicType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file')
            ->add('file', 'file', array(
                'required'  => false,
                'label'  => 'Schematic (généré avec la version spécifiée de WorldEdit)',
            ))
            ->add('save', 'submit', array(
                'label' => 'Uploader'
            ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mineservexpo_mainbundle_schematic';
    }
}