<?php

namespace MineServExpo\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParticipationConcoursType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username2', 'text', array(
                'required'  => false,
                'label'  => 'Pseudo Minecraft du joueur n°2 (compte premium seulement)',
                'attr' => array(
                    'placeholder' => 'Joueur 2',
                    'class' => 'user_skin',
                ),
            ))
            ->add('username3', 'text', array(
                'required'  => false,
                'label'  => 'Pseudo Minecraft du joueur n°3 (compte premium seulement)',
                'attr' => array(
                    'placeholder' => 'Joueur 3',
                    'class' => 'user_skin',
                ),
            ))
            ->add('username4', 'text', array(
                'required'  => false,
                'label'  => 'Pseudo Minecraft du joueur n°4 (compte premium seulement)',
                'attr' => array(
                    'placeholder' => 'Joueur 4',
                    'class' => 'user_skin',
                ),
            ))
            ->add('name', 'text', array(
                'required'  => true,
                'label'  => 'Nom de l\'équipe*',
            ))
            ->add('save', 'submit', array(
                'label' => 'Envoyer'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MineServExpo\MainBundle\Entity\ParticipationConcours'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mineservexpo_mainbundle_participation_concours';
    }
}
