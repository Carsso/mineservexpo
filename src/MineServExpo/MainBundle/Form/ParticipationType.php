<?php

namespace MineServExpo\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParticipationType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', array(
                'choices'   => array(
                    '0' => 'Serveur',
                    '1' => 'Youtubeur',
                    '2' => 'Site Web',
                ),
                'empty_value' => false,
                'required'  => true,
                'label'  => 'Type d\'inscription',
            ))
            ->add('name', 'text', array(
                'required'  => true,
                'label'  => 'Nom',
            ))
            ->add('description', 'textarea', array(
                'required'  => true,
                'label'  => 'Description',
            ))
            ->add('banner', 'text', array(
                'required'  => true,
                'label'  => 'URL de votre bannière en 530px*150px (elle sera utilisée sur le site)',
            ))
            ->add('serverType', 'choice', array(
                'choices'   => array(
                    '0' => 'Industriel',
                    '1' => 'P.V.P.',
                    '2' => 'R.P.',
                    '3' => 'Semi-R.P.',
                    '4' => 'Free Build/Créatif',
                    '5' => 'Survie',
                    '6' => 'Autre',
                ),
                'empty_value' => false,
                'required'  => false,
                'label'  => 'Type de serveur',
            ))
            ->add('website', 'text', array(
                'required'  => false,
                'label'  => 'Site internet (sans http://)',
                'attr' => array(
                    'placeholder' => 'ex: www.monsite.com',
                ),
            ))
            ->add('youtube', 'text', array(
                'required'  => false,
                'label'  => 'Chaine Youtube (Pseudo seulement)',
                'attr' => array(
                    'placeholder' => 'ex: machaine (pour http://www.youtube.com/machaine)',
                ),
            ))
            ->add('facebook', 'text', array(
                'required'  => false,
                'label'  => 'Page Facebook (Alias seulement)',
                'attr' => array(
                    'placeholder' => 'ex: mapage (pour https://www.facebook.com/mapage)',
                ),
            ))
            ->add('twitter', 'text', array(
                'required'  => false,
                'label'  => 'Profil Twitter (Pseudo seulement)',
                'attr' => array(
                    'placeholder' => 'ex: monprofil (pour http://twitter.com/monprofil)',
                ),
            ))
            ->add('audioType', 'choice', array(
                'choices'   => array(
                    '0' => 'Teamspeak',
                    '1' => 'Mumble',
                ),
                'empty_value' => false,
                'required'  => false,
                'label'  => 'Type de serveur audio',
            ))
            ->add('audio', 'text', array(
                'required'  => false,
                'label'  => 'IP du serveur audio',
                'attr' => array(
                    'placeholder' => 'ex: ts.monsite.com:12345',
                ),
            ))
            ->add('save', 'submit', array(
                'label' => 'Envoyer'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MineServExpo\MainBundle\Entity\Participation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mineservexpo_mainbundle_participation';
    }
}
