var form_valid = true;

function renderSkin(username, img) {
    if (username != "") {
        var fos_route = Routing.generate('face_user', { user: username });
        img.attr('src', fos_route);
    } else {
        var fos_route = Routing.generate('face');
        img.attr('src', fos_route);
    }
}

function checkPremium() {
    form_valid = true;
    var user_input = $('input.user_skin, input#fos_user_registration_form_username');
    user_input.each(function (index) {
        var input = $(this);
        var login_mcskin = input.parent('div').children('div.login_avatar').children('img#login_mcskin');
        login_mcskin.attr('src', '');
        var username = input.val();
        input.css('background-color', 'none');
        if (username) {
            $.ajax({
                url: Routing.generate('minecraft_premium_user', { user: username }),
                dataType: "json",
                success: function (data) {
                    switch (data.result) {
                        case 1:
                            input.css('background-color', '#ccffcc');
                            renderSkin(username, login_mcskin);
                            break;
                        case -1:
                            input.css('background-color', '#ccccff');
                            renderSkin(username, login_mcskin);
                            break;
                        case 0:
                        default:
                            form_valid = false;
                            input.css('background-color', '#ffcccc');
                            renderSkin(username, login_mcskin);
                            break;
                    }
                },
                error: function () {
                    input.css('background-color', '#ffcccc');
                    renderSkin('', login_mcskin);
                }
            });
        } else {
            renderSkin('', login_mcskin);
        }
    });
}

function renderBanner(input) {
    var fos_username = input.val();
    if (fos_username != "") {
        $('#img_perview_banner').attr('src', fos_username);
    } else {
        if (typeof defaultBannerUrl !== "undefined") {
            $('#img_perview_banner').attr('src', defaultBannerUrl);
        } else {
            $('#img_perview_banner').attr('src', '');
        }
    }
}

function renderServerType(input) {
    var part_server_type_val = input.val();
    if (part_server_type_val == "0") {
        $('#mineservexpo_mainbundle_participation_serverType').parent('div').show();
    } else {
        $('#mineservexpo_mainbundle_participation_serverType').parent('div').hide();
    }
}

function checkHost(link){
    if(link.indexOf('dedishops.com') != -1){
        return false;
    }
    if(link.indexOf('dedizones.com') != -1){
        return false;
    }
    if(link.indexOf('http://www.mineserv-exposition.fr') != -1){
        return false;
    }
    if(link.indexOf('http://mineservexpo.local') != -1){
        return false;
    }
    if(link.indexOf('ts3server://') != -1){
        return false;
    }
    if(link.indexOf('twitter.com') != -1){
        return false;
    }
    if(link.indexOf('youtube.com') != -1){
        return false;
    }
    if(link.indexOf('facebook.com') != -1){
        return false;
    }
    return true;
}


$(document).ready(function () {
    var skin_timeout;
    var user_input = $('input.user_skin, input#fos_user_registration_form_username');
    user_input.each(function (index) {
        var skin_create_div = $('<div class="login_avatar"><img src="" id="login_mcskin" /></div>');
        $(this).parent('div').prepend(skin_create_div);
        $(this).change(function () {
            clearTimeout(skin_timeout);
            skin_timeout = setTimeout(function () {
                checkPremium();
            }, 1000);
        });
        $(this).keyup(function () {
            clearTimeout(skin_timeout);
            skin_timeout = setTimeout(function () {
                checkPremium();
            }, 1000);
        });
        checkPremium();
    });
    var formulaire_error = $('<div class="mt20 error_msg"></div>');
    $('[type=submit]').parent('div').prepend(formulaire_error);

    $('form').submit(function () {
        if (!form_valid) {
            var formulaire_error = '<div class="banner banner-red">Merci de remplir correctement le formulaire</div>';
            $('.error_msg').html(formulaire_error);
            return false;
        }
    });

    // Participation server type
    var part_server_type = $('#mineservexpo_mainbundle_participation_type');
    if (part_server_type) {
        part_server_type.change(function () {
            renderServerType($(this));
        });
        renderServerType(part_server_type);
    }

    // Preview banner
    var perview_banner = $('#mineservexpo_mainbundle_participation_banner');
    if (perview_banner) {
        var perview_timeout;
        var create_div = $('<div class="banner"><img src="" id="img_perview_banner" width="530" height="150" /></div>');
        perview_banner.after(create_div);
        perview_banner.change(function () {
            var that = $(this);
            clearTimeout(perview_timeout);
            perview_timeout = setTimeout(function () {
                renderBanner(that);
            }, 1000);
        });
        renderBanner(perview_banner);
    }

    $('textarea').autosize({append: "\n"});


    $('#search_form').submit(function () {
        document.location = Routing.generate('exposants_search', {'q': $('input[name=q]').val(), 't': $('input[name=t]').val() });
        return false;
    });
    $(":file").filestyle({buttonText: "Choisir un fichier"});

    $('a').click(function () {
        if (checkHost(this.href)) {
            var new_href = encodeURIComponent(this.href).replace(/%20/g, '+');
            var redir_route = Routing.generate('redirect', { url: new_href });
            this.href = redir_route;
        }
    });
});