<?php

namespace MineServExpo\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToMany(targetEntity="MineServExpo\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="fos_user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**
     * @ORM\OneToMany(targetEntity="MineServExpo\MainBundle\Entity\Participation", mappedBy="user")
     */
    protected $participations;

    /**
     * @ORM\OneToOne(targetEntity="MineServExpo\MainBundle\Entity\ParticipationConcours", mappedBy="user")
     */
    protected $participation_concours;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->participations = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Add participations
     *
     * @param \MineServExpo\MainBundle\Entity\Participation $participations
     * @return User
     */
    public function addParticipation(\MineServExpo\MainBundle\Entity\Participation $participations)
    {
        $this->participations[] = $participations;
    
        return $this;
    }

    /**
     * Remove participations
     *
     * @param \MineServExpo\MainBundle\Entity\Participation $participations
     */
    public function removeParticipation(\MineServExpo\MainBundle\Entity\Participation $participations)
    {
        $this->participations->removeElement($participations);
    }

    /**
     * Get participations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParticipations()
    {
        return $this->participations;
    }

    /**
     * Set participation_concours
     *
     * @param \MineServExpo\MainBundle\Entity\ParticipationConcours $participationConcours
     * @return User
     */
    public function setParticipationConcours(\MineServExpo\MainBundle\Entity\ParticipationConcours $participationConcours = null)
    {
        $this->participation_concours = $participationConcours;

        return $this;
    }

    /**
     * Get participation_concours
     *
     * @return \MineServExpo\MainBundle\Entity\ParticipationConcours 
     */
    public function getParticipationConcours()
    {
        return $this->participation_concours;
    }
}
